# UTF-8 test:äëïöü
# Copyright (C) 2001 Free Software Foundation, Inc.
# Juanita Franz <juanita.franz@vr-web.de>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: kio_fish stable\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-07-08 00:38+0000\n"
"PO-Revision-Date: 2005-06-08 01:39+0200\n"
"Last-Translator: Juanita Franz <juanita.franz@vr-web.de>\n"
"Language-Team: AFRIKAANS <translate-discuss-af@lists.sourceforge.net>\n"
"Language: af\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: fish.cpp:316
#, kde-format
msgid "Connecting..."
msgstr "Besig om te koppel..."

#: fish.cpp:642
#, kde-format
msgid "Initiating protocol..."
msgstr "Inisialiseer protokol..."

#: fish.cpp:679
#, kde-format
msgid "Local Login"
msgstr "Plaaslike Aanteken"

#: fish.cpp:681
#, fuzzy, kde-format
#| msgid "SSH Authorization"
msgid "SSH Authentication"
msgstr "SSH Magtiging"

#: fish.cpp:718 fish.cpp:733
#, kde-format
msgctxt "@action:button"
msgid "Yes"
msgstr ""

#: fish.cpp:718 fish.cpp:733
#, kde-format
msgctxt "@action:button"
msgid "No"
msgstr ""

#: fish.cpp:816
#, kde-format
msgid "Disconnected."
msgstr "Ontkoppel."
