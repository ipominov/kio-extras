# translation of kio_thumbnail.po to Chinese Traditional
#
# Frank Weng (a.k.a. Franklin) <franklin at goodhorse dot idv dot tw>, 2006, 2007, 2009.
# Franklin Weng <franklin at goodhorse dot idv dot tw>, 2007.
# Franklin Weng <franklin@goodhorse.idv.tw>, 2011.
# SPDX-FileCopyrightText: 2024 Kisaragi Hiu <mail@kisaragi-hiu.com>
msgid ""
msgstr ""
"Project-Id-Version: kio_thumbnail\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-11-19 00:40+0000\n"
"PO-Revision-Date: 2024-05-08 14:32+0900\n"
"Last-Translator: Kisaragi Hiu <mail@kisaragi-hiu.com>\n"
"Language-Team: Traditional Chinese <zh-l10n@lists.slat.org>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 24.04.70\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: thumbnail.cpp:199
#, kde-format
msgid "No MIME Type specified."
msgstr "沒有指定 MIME 型態。"

#: thumbnail.cpp:206
#, kde-format
msgid "No or invalid size specified."
msgstr "沒有指定大小，或指定的大小不對。"

#: thumbnail.cpp:227
#, kde-format
msgid "Cannot create thumbnail for directory"
msgstr "無法產生目錄的縮圖"

#: thumbnail.cpp:236
#, kde-format
msgid "No plugin specified."
msgstr "沒有指定外掛。"

#: thumbnail.cpp:241
#, kde-format
msgid "Cannot load ThumbCreator %1"
msgstr "無法載入 ThumbCreator %1"

#: thumbnail.cpp:249
#, kde-format
msgid "Cannot create thumbnail for %1"
msgstr "無法產生 %1 的縮圖"

#: thumbnail.cpp:259
#, kde-format
msgid "Failed to create a thumbnail."
msgstr "產生縮圖失敗。"

#: thumbnail.cpp:279
#, kde-format
msgid "Could not write image."
msgstr "無法寫入影像。"

#: thumbnail.cpp:307
#, kde-format
msgid "Failed to attach to shared memory segment %1"
msgstr "無法連結共享記憶體區段 %1"

#: thumbnail.cpp:311
#, kde-format
msgid "Image is too big for the shared memory segment"
msgstr "影像太大無法放進共享記憶體區段"

#~ msgctxt "@option:check"
#~ msgid "Rotate the image automatically"
#~ msgstr "自動旋轉影像"
